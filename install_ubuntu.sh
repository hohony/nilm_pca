﻿#Installing nodejs v6.11.3 
sudo apt-get install curl
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

# Installing npm 
sudo apt-get install -y build-essential
sudo apt-get install npm 
sudo npm install mongoose 

# Installing mongodb-enterprise 
#sudo apt-get install -y mongodb-enterprise
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
#Create a /etc/apt/sources.list.d/mongodb-enterprise.list file for MongoDB.
echo "deb [ arch=amd64,arm64,ppc64el,s390x ] http://repo.mongodb.com/apt/ubuntu xenial/mongodb-enterprise/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-enterprise.list
sudo apt-get update
sudo apt-get install –y mongodb-enterprise

sudo mkdir /data
sudo mkdir /data/db

# Installing python3 
sudo apt-get install python3.5
pip3 install numpy scipy pandas pymongo pyserial
pip3 install -U scikit-learn=-0.19.1

# Adding ttyACM module to Nvidia Jetson TX
# git clone https://github.com/jetsonhacks/installACMModule
# cd installACMModule 
# sudo ./installCDCACM.sh
# uname -r 
# lsmod
# sudo modprobe cdc-acm
# lsmod
# ls /dev/ttyACM* 
