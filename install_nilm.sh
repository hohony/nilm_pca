#!/bin/sh

# build essential
yum -y update
echo "Install essential"
yum install -y glibc-devel make
yum groupinstall "Development Tools"
yum install kernel-devel kernel-headers

# nodejs
echo "Install nodejs"
yum install curl
curl -sL https://deb.nodesource.com/setup_6.x | -E bash -
yum install -y nodejs

# mongodb-enterprise
echo "Install mongodb"
mv ./repo/mongodb-enterprise.repo /etc/yum.repos.d/
yum install -y mongodb-enterprise

echo "Regist excluding inform about mongodb auto update"
echo "exclude=mongodb-enterprise,mongodb-enterprise-server,mongodb-enterprise-shell,mongodb-enterprise-mongos,mongodb-enterprise-tools" |  tee -a /etc/yum.conf
mkdir -p /data/db

# npm 
echo "Install npm"
yum install npm
npm install mongoose 

#python3 install
echo "Installing python3"
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum update
yum install -y python35u python35u-libs python35u-devel python35u-pip python35u-setuptools

# python3.5 library
echo "Install python3 module"
yum install -y gcc gcc-c++ 
pip3.5 install numpy scipy pandas pymongo pyserial
pip3.5 install -U scikit-learn==0.19.1 

#ttyACM
echo "Install picocom"
yum install picocom
