import serial
import serial.tools.list_ports
import time
import numpy as np
import pandas as pd
from time import gmtime, strftime
import datetime

#print(list(serial.tools.list_ports.comports()))

class MCP39F511_SingleWireMode:
	def __init__(self):
		self.success_count = 0
		self.fail_count = 0
		pass

	def print_success_failure(self):
		print("success : " + str(self.success_count))
		print("failure : " + str(self.fail_count))

	def reset_success_failure_count(self):
		self.success_count = 0
		self.fail_count = 0

	def check_data(self, data_array):
		if len(data_array) != 20:
			#print("data length is not 20!")
			self.fail_count += 1
			return False
		if len(data_array) == 20:
			if(data_array[0]) == b"/xAB":
				#print("header1 is not 0xAB")
				self.fail_count += 1
				return False
			if(data_array[1]) == b"/xCD":
				#print("header2 is not 0xCD")
				self.fail_count += 1
				return False
			if(data_array[2]) == b"/xEF":
				#print("header3 is not 0xEF")
				self.fail_count += 1
				return False
		#print("Success")
		self.success_count += 1
		return True

	def data_read(self, data_array):
		self.HeaderByte_1 = data_array[0]
		self.HeaderByte_2 = data_array[1]
		self.HeaderByte_2 = data_array[2]
		self.Current_LL = data_array[3]
		self.Current_L = data_array[4]
		self.Current_H = data_array[5]
		self.Current_HH = data_array[6]
		self.Voltage_L = data_array[7]
		self.Voltage_H = data_array[8]
		self.ActivePwr_LL = data_array[9]
		self.ActivePwr_L = data_array[10]
		self.ActivePwr_H = data_array[11]
		self.ActivePwr_HH = data_array[12]
		self.ReactvPwr_LL = data_array[13]
		self.ReactvPwr_L = data_array[14]
		self.ReactvPwr_H = data_array[15]
		self.ReactvPwr_HH = data_array[16]
		self.Frequency_L = data_array[17]
		self.Frequency_H = data_array[18]
		self.CheckSum = data_array[19]

	def transform_data(self):
		#self.SysStatus = hex((int(self.SysStatus_H, 16) << 8) + int(self.SysStatus_L,16))
		#self.Version =  hex((int(self.SysVer_H, 16) << 8) + int(self.SysVer_L,16))
		self.Current = ((self.Current_HH << 24) + (self.Current_H << 16) + (self.Current_L << 8) + (self.Current_LL))/10000.0
		self.Voltage = ((self.Voltage_H << 8) + self.Voltage_L)/10.0
		self.ActivePwr = ((self.ActivePwr_HH << 24) + (self.ActivePwr_H << 16) + (self.ActivePwr_L << 8) + (self.ActivePwr_LL))/100.0
		self.ReactvPwr = ((self.ReactvPwr_HH << 24) + (self.ReactvPwr_H << 16) + (self.ReactvPwr_L << 8) + (self.ReactvPwr_LL))/100.0
		self.Frequency = ((self.Frequency_H << 8) + self.Frequency_L)/1000.0
		#self.AnalogIn = (((int(self.AnalogIn_H, 16) << 8) + int(self.Frequency_L, 16))&0x03FF)/10.0*1.8+32
		#self.PwrFactor = ((int(self.PwrFactor_H, 16) << 8) + int(self.PwrFactor_L,16))*0.000030517578125
		#self.ApprntPwr = ((int(self.ApprntPwr_HH, 16) << 24) + (int(self.ApprntPwr_H,16) << 16) + (int(self.ApprntPwr_L,16) << 8) + (int(self.ApprntPwr_LL,16)))/100.0

	def get_feature(self):
		return [self.Current, self.Voltage, self.ActivePwr, self.ReactvPwr, self.Frequency]

	def print_feature(self):
		print("-------------------------------------------------------------------")
		print("MCP39F511 DEMO BOARD")
		print("-------------------------------------------------------------------")
		print("OUTPUTS")
		print("RMS Current : %4.6f" %self.Current)
		print("RMS Voltage : %4.2f" %self.Voltage)
		print("Active Power : %4.2f" %self.ActivePwr)
		print("Reactive Power : %4.2f" %self.ReactvPwr)
		print("Frequency : %4.2f" %self.Frequency)
		#print("Apparent Power : %4.2f" %self.ApprntPwr)
		#print("Temperature : %4.2f" %self.AnalogIn)
		#print("Power Factor : %4.2f" %self.PwrFactor)
		print("")

	def print_raw_data(self):
		pass
		#print("Header : %s" %self.Header)
		#print("Byte_Count : %s" %self.Byte_Count)
		#print("SysStatus_L : %s" %self.SysStatus_L)
		#print("SysStatus_H : %s" %self.SysStatus_H)
		#print("SysVer_L : %s" %self.SysVer_L)
		#print("SysVer_H : %s" %self.SysVer_H)
		#print("Voltage_L : %s" %self.Voltage_L)
		#print("Voltage_H : %s" %self.Voltage_H)
		#print("Frequency_L : %s" %self.Frequency_L)
		#print("Frequency_H : %s" %self.Frequency_H)
		#print("AnalogIn_L : %s" %self.AnalogIn_L)
		#print("AnalogIn_H : %s" %self.AnalogIn_H)
		#print("PwrFactor_L : %s" %self.PwrFactor_L)
		#print("PwrFactor_H : %s" %self.PwrFactor_H)
		#print("Current_LL : %s" %self.Current_LL)
		#print("Current_L : %s" %self.Current_L)
		#print("Current_H : %s" %self.Current_H)
		#print("Current_HH : %s" %self.Current_HH)
		#print("ActivePwr_LL : %s" %self.ActivePwr_LL)
		#print("ActivePwr_L : %s" %self.ActivePwr_L)
		#print("ActivePwr_H : %s" %self.ActivePwr_H)
		#print("ActivePwr_HH : %s" %self.ActivePwr_HH)
		#print("ReactvPwr_LL : %s" %self.ReactvPwr_LL)
		#print("ReactvPwr_L : %s" %self.ReactvPwr_L)
		#print("ReactvPwr_H : %s" %self.ReactvPwr_H)
		#print("ReactvPwr_HH : %s" %self.ReactvPwr_HH)
		#print("ApprntPwr_LL : %s" %self.ApprntPwr_LL)
		#print("ApprntPwr_L : %s" %self.ApprntPwr_L)
		#print("ApprntPwr_H : %s" %self.ApprntPwr_H)
		#print("ApprntPwr_HH : %s" %self.ApprntPwr_HH)
		#print("CheckSum : %s" %self.CheckSum)

	def run(self, data_array):

		res = []

		if(self.check_data(data_array)):
			self.data_read(data_array)
			self.transform_data()
			res = self.get_feature()
		else:
			res = False
		return res

def ser_read(ser):
	data_array = []	
	for line in ser.read(100):
		data_array.append(line)
	return data_array

def ser_write_single_mode(ser):
	sleepTime = 0.1
	ser.write(b"\xA5")
	time.sleep(sleepTime)
	ser.write(b"\x0C")
	time.sleep(sleepTime)
	ser.write(b"\x41")
	time.sleep(sleepTime)
	ser.write(b"\x00")
	time.sleep(sleepTime)
	ser.write(b"\x7A")
	time.sleep(sleepTime)
	ser.write(b"\x4D")
	time.sleep(sleepTime)
	ser.write(b"\x04")
	time.sleep(sleepTime)
	ser.write(b"\x00")
	time.sleep(sleepTime)
	ser.write(b"\x01")
	time.sleep(sleepTime)
	ser.write(b"\x00")
	time.sleep(sleepTime)
	ser.write(b"\x03")
	time.sleep(sleepTime)
	ser.write(b"\xC1")
	time.sleep(sleepTime)
	data_array = []	
	for line in ser.read(100):
		data_array.append(line)
    
	#print(data_array)
	return data_array

def make_current_filename(root, filename):
	filepath = root + filename +"_"+datetime.datetime.now().strftime('%Y%m%d_%H:%M:%S')+".csv"
	return filepath

def main():

	#csv file save path
	root = "../data/data_single_wire_mode_jetson/"
	current_file = "test"
	columns = ["Year", "Month", "Day", "Hour", "Minute", "Second", "Millisecond", "Current", "Voltage", "ActivePwr", "ReactvPwr", "Frequency"]

	#serial connect
	ser = serial.Serial(port='/dev/ttyACM0',baudrate=115200, timeout=0.01)
	#connect
	print("connected to : " + ser.portstr)
	#set single wire mode
	print("set single wire mode:")
	ACK = ser_write_single_mode(ser)
	if len(ACK) == 0:
		print("fail... data length is 0")
		return False

	#instance call
	mydata = MCP39F511_SingleWireMode()
	#get data from device
	for j in range(1): 
		
		#filename = make_current_filename(root, current_file)
		#filename=root+"Samsung_Fan_Airpurifier_Hairdryer_Steady_LG_Transient.csv"
		filename=root+"fan_mixer_hairdryer3.csv"
		#Dataframe Attribute
		mydata_frame = pd.DataFrame(columns=columns)
		#Write Attribute
		mydata_frame.to_csv(filename, index=False)
		mydata_set = []
		
		start = time.time()

		for i in range(6000):
			
			processed_data = mydata.run(ser_read(ser))
			if processed_data != False:
				c_time = datetime.datetime.now().strftime('%Y:%m:%d:%H:%M:%S:%f').split(":")
				mydata_set.append(c_time + processed_data)
				#print(processed_data)
				mydata.print_feature()
			else:
				pass

			#Write Data
			if i % 100 == 99 :
				
				mydata_frame = pd.DataFrame(mydata_set,columns=columns)
				mydata_frame.to_csv(filename, mode="a", header=False, index=False)
				mydata_set = []

		end = time.time() - start
		print("during time : %f" %end)
		mydata.print_success_failure()
		mydata.reset_success_failure_count()

	ser.close()

if __name__ == "__main__":
	main()
