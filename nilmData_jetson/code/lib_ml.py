
# coding: utf-8

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf

def labeling(path=None, low=0, high=1, threshold_index=9, save_path=None):    
    df = pd.read_csv(path)
    current_data = df["Current"]
    n, bins, patches = plt.hist(current_data, 500, facecolor='g')
    threshold = bins[threshold_index]
    labels = []
    for i in range(0, len(current_data)):
        if current_data[i] >= threshold:
            labels.append(high)
        else:
            labels.append(low)
    df["State"] = pd.Series(labels, index=df.index)
    
    x1 = df["Current"]
    x2 = df["State"]
    x_max = max(max(x1), max(x2))
    x_max = x_max*1.3
    
    plt.figure(1)
    plt.plot(x1)
    plt.plot(x2)
    plt.title("Current_State")
    plt.axis([0,len(x1), 0, x_max])

    df.to_csv(save_path, index=False)
    return plt


def print_all_data(path, features, save_path):
    
    plt.figure(1)
    
    df = pd.read_csv(path) 
    subplot_num = len(features)
    
    for i in range(len(features)):
        
        x = df[features[i]]
        plt.subplot(subplot_num,1,i+1)
        plt.plot(x)
        plt.title(features[i])
        plt.axis([0,len(x), 0, max(x)+max(x)*0.3])
        

    plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=1.0, wspace=0.35)
    #plt.show()
    plt.savefig(save_path +".png")



def make_one_hot(array_size, onehot_index):
    tmp_array = np.zeros(array_size)
    tmp_array[onehot_index]=1.0
    return tmp_array.tolist()




def Combine_labeling(path, array_size, onehot_index, save_path):    
    data_df = pd.read_csv(root_path+path)
    state = data_df["State"]
    
    onehot_labels = []
    for i in range(0,len(state)):
        if state[i] == 0:
            onehot_labels.append(make_one_hot(array_size,0))
        else:
            onehot_labels.append(make_one_hot(array_size,onehot_index))
    
    label_df = pd.DataFrame(onehot_labels, columns=colums)

    concat_df = pd.concat([data_df, label_df], axis=1, join_axes=[data_df.index])
    del concat_df["State"]
    concat_df.to_csv(save_path, header=True, index=False)




def make_dataset(files, features, labels, ratio):
    
    train_X_dataset = pd.DataFrame(columns=features)
    train_Y_label = pd.DataFrame(columns=labels)
    test_X_dataset = pd.DataFrame(columns=features)
    test_Y_label = pd.DataFrame(columns=labels)
    
    for i in files:
        data_df = pd.read_csv(i)
        dlim = int(len(data_df) * ratio)
        train_X_dataset = pd.concat([train_X_dataset,data_df[:dlim][features]], ignore_index=True)
        train_Y_label = pd.concat([train_Y_label,data_df[:dlim][labels]], ignore_index=True)
        test_X_dataset = pd.concat([test_X_dataset,data_df[dlim:][features]], ignore_index=True)
        test_Y_label = pd.concat([test_Y_label,data_df[dlim:][labels]], ignore_index=True)
        
    
    return train_X_dataset.values, train_Y_label.values, test_X_dataset.values, test_Y_label.values


def reshape_for_data(data, sequence, input_dim):
    rest = int(len(data) % (sequence))
    _data = data[:-rest]
    return np.reshape(_data,(-1,sequence,input_dim))


def reshape_for_label(data, sequence):
    rest = int(len(data) % (sequence))
    _data = data[:-rest]
    return np.reshape(_data,(-1,sequence))


def error_matrix(predict, target,index):
    target_index = []
    predict_index = []
    
    TP = 0
    TN = 0
    FP = 0
    FN = 0
    
    target_flat = np.reshape(target, (-1))
    predict_flat = np.reshape(predict, (-1))
    
    for i in range(len(target_flat)):
        if target_flat[i] == index or predict_flat[i] == index:
            target_index.append(target_flat[i])
            predict_index.append(predict_flat[i])
    
    for i in range(len(target_index)):
        if target_index[i] == index and  predict_index[i] == index:
            TP += 1
        elif target_index[i] == index and predict_index[i] != index:
            FN += 1
        elif target_index[i] != index and predict_index[i] == index:
            FP += 1
    
    #print("TP, TN, FP, FN", TP, TN, FP, FN)
    if len(target_index) != 0:
        accuracy = float(TP) / float(len(target_index))
    else :
        accuracy = 0
        
    if TP+FP != 0:
        precision = float(TP) / float(TP+FP)
    else:
        precision = 0
    
    if TP+FN != 0:
        recall = float(TP) / float(TP+FN)
    else:
        recall = 0
        
    if precision+recall != 0:
        f1 =  2*(float(precision*recall) / float(precision+recall))
    else:
        f1 = 0
        
    return accuracy, precision, recall, f1






