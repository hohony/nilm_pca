
# coding: utf-8

# In[ ]:


import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt
#import tensorflow as tf
import serial
#import serial.tools.list_ports
import time
from time import gmtime, strftime
import datetime
from pymongo import MongoClient

from pandas import Series
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.decomposition import PCA
#from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.externals import joblib

#%matplotlib notebook


# In[ ]:


#train data
data_path = "../data/data_single_wire_mode_jetson/labeled/"

Fan = "fan.csv"
Mixer = "mixer.csv"
Hairdryer1 = "hairdryer1.csv"
Hairdryer2 = "hairdryer2.csv"
Hairdryer3 = "hairdryer3.csv"
Fan_Mixer = "fan_mixer.csv"
Fan_Hairdryer1 = "fan_hairdryer1.csv"
Fan_Hairdryer2 = "fan_hairdryer2.csv"
Fan_Hairdryer3 = "fan_hairdryer3.csv"
Mixer_Hairdryer1 = "mixer_hairdryer1.csv"
Mixer_Hairdryer2 = "mixer_hairdryer2.csv"
Mixer_Hairdryer3 = "mixer_hairdryer3.csv"
Fan_Mixer_Hairdryer1 = "fan_mixer_hairdryer1.csv"
Fan_Mixer_Hairdryer2 = "fan_mixer_hairdryer2.csv"
Fan_Mixer_Hairdryer3 = "fan_mixer_hairdryer3.csv"


file_list = [Fan, 
             Mixer, 
             Hairdryer1, 
             Hairdryer2, 
             Hairdryer3,
             Fan_Mixer,
             Fan_Hairdryer1,
             Fan_Hairdryer2,
             Fan_Hairdryer3,
             Mixer_Hairdryer1,
             Mixer_Hairdryer2,
             Mixer_Hairdryer3,
             Fan_Mixer_Hairdryer1,
             Fan_Mixer_Hairdryer2,
             Fan_Mixer_Hairdryer3
            ]

files = [data_path + s for s in file_list]
features = ["ActivePwr", "ReactvPwr"]
labels=["State"]


# In[ ]:


#print(list(serial.tools.list_ports.comports()))

class MCP39F511_SingleWireMode:
	def __init__(self):
		self.success_count = 0
		self.fail_count = 0
		pass

	def print_success_failure(self):
		print("success : " + str(self.success_count))
		print("failure : " + str(self.fail_count))

	def reset_success_failure_count(self):
		self.success_count = 0
		self.fail_count = 0

	def check_data(self, data_array):
		if len(data_array) != 20:
			#print("data length is not 20!")
			self.fail_count += 1
			return False
		if len(data_array) == 20:
			if(data_array[0]) == b"/xAB":
				#print("header1 is not 0xAB")
				self.fail_count += 1
				return False
			if(data_array[1]) == b"/xCD":
				#print("header2 is not 0xCD")
				self.fail_count += 1
				return False
			if(data_array[2]) == b"/xEF":
				#print("header3 is not 0xEF")
				self.fail_count += 1
				return False
		#print("Success")
		self.success_count += 1
		return True

	def data_read(self, data_array):
		self.HeaderByte_1 = data_array[0]
		self.HeaderByte_2 = data_array[1]
		self.HeaderByte_2 = data_array[2]
		self.Current_LL = data_array[3]
		self.Current_L = data_array[4]
		self.Current_H = data_array[5]
		self.Current_HH = data_array[6]
		self.Voltage_L = data_array[7]
		self.Voltage_H = data_array[8]
		self.ActivePwr_LL = data_array[9]
		self.ActivePwr_L = data_array[10]
		self.ActivePwr_H = data_array[11]
		self.ActivePwr_HH = data_array[12]
		self.ReactvPwr_LL = data_array[13]
		self.ReactvPwr_L = data_array[14]
		self.ReactvPwr_H = data_array[15]
		self.ReactvPwr_HH = data_array[16]
		self.Frequency_L = data_array[17]
		self.Frequency_H = data_array[18]
		self.CheckSum = data_array[19]

	def transform_data(self):
		#self.SysStatus = hex((int(self.SysStatus_H, 16) << 8) + int(self.SysStatus_L,16))
		#self.Version =  hex((int(self.SysVer_H, 16) << 8) + int(self.SysVer_L,16))
		self.Current = ((self.Current_HH << 24) + (self.Current_H << 16) + (self.Current_L << 8) + (self.Current_LL))/10000.0
		self.Voltage = ((self.Voltage_H << 8) + self.Voltage_L)/10.0
		self.ActivePwr = ((self.ActivePwr_HH << 24) + (self.ActivePwr_H << 16) + (self.ActivePwr_L << 8) + (self.ActivePwr_LL))/100.0
		self.ReactvPwr = ((self.ReactvPwr_HH << 24) + (self.ReactvPwr_H << 16) + (self.ReactvPwr_L << 8) + (self.ReactvPwr_LL))/100.0
		self.Frequency = ((self.Frequency_H << 8) + self.Frequency_L)/1000.0
		#self.AnalogIn = (((int(self.AnalogIn_H, 16) << 8) + int(self.Frequency_L, 16))&0x03FF)/10.0*1.8+32
		#self.PwrFactor = ((int(self.PwrFactor_H, 16) << 8) + int(self.PwrFactor_L,16))*0.000030517578125
		#self.ApprntPwr = ((int(self.ApprntPwr_HH, 16) << 24) + (int(self.ApprntPwr_H,16) << 16) + (int(self.ApprntPwr_L,16) << 8) + (int(self.ApprntPwr_LL,16)))/100.0

	def get_feature(self):
		return [self.Current, self.Voltage, self.ActivePwr, self.ReactvPwr, self.Frequency]

	def print_feature(self):
		print("-------------------------------------------------------------------")
		print("MCP39F511 DEMO BOARD")
		print("-------------------------------------------------------------------")
		print("OUTPUTS")
		print("RMS Current : %4.6f" %self.Current)
		print("RMS Voltage : %4.2f" %self.Voltage)
		print("Active Power : %4.2f" %self.ActivePwr)
		print("Reactive Power : %4.2f" %self.ReactvPwr)
		print("Frequency : %4.2f" %self.Frequency)
		#print("Apparent Power : %4.2f" %self.ApprntPwr)
		#print("Temperature : %4.2f" %self.AnalogIn)
		#print("Power Factor : %4.2f" %self.PwrFactor)
		print("")

	def print_raw_data(self):
		pass
		#print("Header : %s" %self.Header)
		#print("Byte_Count : %s" %self.Byte_Count)
		#print("SysStatus_L : %s" %self.SysStatus_L)
		#print("SysStatus_H : %s" %self.SysStatus_H)
		#print("SysVer_L : %s" %self.SysVer_L)
		#print("SysVer_H : %s" %self.SysVer_H)
		#print("Voltage_L : %s" %self.Voltage_L)
		#print("Voltage_H : %s" %self.Voltage_H)
		#print("Frequency_L : %s" %self.Frequency_L)
		#print("Frequency_H : %s" %self.Frequency_H)
		#print("AnalogIn_L : %s" %self.AnalogIn_L)
		#print("AnalogIn_H : %s" %self.AnalogIn_H)
		#print("PwrFactor_L : %s" %self.PwrFactor_L)
		#print("PwrFactor_H : %s" %self.PwrFactor_H)
		#print("Current_LL : %s" %self.Current_LL)
		#print("Current_L : %s" %self.Current_L)
		#print("Current_H : %s" %self.Current_H)
		#print("Current_HH : %s" %self.Current_HH)
		#print("ActivePwr_LL : %s" %self.ActivePwr_LL)
		#print("ActivePwr_L : %s" %self.ActivePwr_L)
		#print("ActivePwr_H : %s" %self.ActivePwr_H)
		#print("ActivePwr_HH : %s" %self.ActivePwr_HH)
		#print("ReactvPwr_LL : %s" %self.ReactvPwr_LL)
		#print("ReactvPwr_L : %s" %self.ReactvPwr_L)
		#print("ReactvPwr_H : %s" %self.ReactvPwr_H)
		#print("ReactvPwr_HH : %s" %self.ReactvPwr_HH)
		#print("ApprntPwr_LL : %s" %self.ApprntPwr_LL)
		#print("ApprntPwr_L : %s" %self.ApprntPwr_L)
		#print("ApprntPwr_H : %s" %self.ApprntPwr_H)
		#print("ApprntPwr_HH : %s" %self.ApprntPwr_HH)
		#print("CheckSum : %s" %self.CheckSum)

	def run(self, data_array):

		res = []

		if(self.check_data(data_array)):
			self.data_read(data_array)
			self.transform_data()
			res = self.get_feature()
		else:
			res = False
		return res

def ser_read(ser):
	data_array = []	
	for line in ser.read(100):
		data_array.append(line)
	return data_array

def ser_write_single_mode(ser):
	sleepTime = 0.1
	ser.write(b"\xA5")
	time.sleep(sleepTime)
	ser.write(b"\x0C")
	time.sleep(sleepTime)
	ser.write(b"\x41")
	time.sleep(sleepTime)
	ser.write(b"\x00")
	time.sleep(sleepTime)
	ser.write(b"\x7A")
	time.sleep(sleepTime)
	ser.write(b"\x4D")
	time.sleep(sleepTime)
	ser.write(b"\x04")
	time.sleep(sleepTime)
	ser.write(b"\x00")
	time.sleep(sleepTime)
	ser.write(b"\x01")
	time.sleep(sleepTime)
	ser.write(b"\x00")
	time.sleep(sleepTime)
	ser.write(b"\x03")
	time.sleep(sleepTime)
	ser.write(b"\xC1")
	time.sleep(sleepTime)
	data_array = []	
	for line in ser.read(100):
		data_array.append(line)
    
	#print(data_array)
	return data_array

def make_current_filename(root, filename):
	filepath = root + filename +"_"+datetime.datetime.now().strftime('%Y%m%d_%H:%M:%S')+".csv"
	return filepath


# In[ ]:


def setPCA(data, save_path=None, isSave = False):
    
    pca_training = PCA().fit(data)
      
    if isSave == True:
        joblib.dump(pca_training, save_path)

    return pca_training

def getPCA(load_path=None):
    pca_training = None
    pca_training = joblib.load(load_path)
    
    return pca_training


# In[ ]:


def setETC(x_data, y_data, save_path=None, isSave = False):
    
    ETC = ExtraTreesClassifier()
    ETC.fit(x_data, y_data)
      
    if isSave == True:
        joblib.dump(ETC, save_path)

    return ETC

def getETC(load_path=None):
    ETC = None
    ETC = joblib.load(load_path)
    
    return ETC


# In[ ]:


PCA_save_path = "../model/save_pca/pca.pkl"
ETC_save_path = "../model/save_etc/etc.pkl"

pca_training = getPCA(PCA_save_path)
ETC = getETC(ETC_save_path)


# In[ ]:


size_x = 210

init_x = np.arange(size_x)
init_y1 = list(np.zeros(size_x))
init_y2 = list(np.zeros(size_x))
init_y3 = list(np.zeros(size_x))
init_y4 = list(np.zeros(size_x))
init_y5 = list(np.zeros(size_x))


# In[ ]:


y1_min_value = 0
y2_min_value = 0
y3_min_value = 0
y4_min_value = 0
y5_min_value = -200

y1_max_value = 7.0 #Current
y2_max_value = 250 #Voltage
y3_max_value = 200 #ActivePwr
y4_max_value = 200 #ReactvPwr
y5_max_value = 200  #Feature


# In[ ]:


key = {0 : "off", 
       1 : "fan", 
       2 : "mixer", 
       3 : "hairdryer1", 
       4 : "hairdryer2", 
       5 : "hairdryer3", 
       6 : "fan_mixer", 
       7 : "fan_hairdryer1",
       8 : "fan_hairdryer2",
       9 : "fan_hairdryer3",
       10 : "mixer_hairdryer1",
       11 : "mixer_hairdryer2",
      12 : "mixer_hairdryer3",
      13 : "fan_mixer_hairdryer1",
      14 : "fan_mixer_hairdryer2",
      15 : "fan_mixer_hairdryer3",
      }


# In[ ]:


client = MongoClient("localhost", 27017)
db = client.energy_data
collection = db.energy_collection
posts = db.posts


# In[ ]:


#csv file save path
#root = "../data/data_single_wire_mode/"
#current_file = "test"
#columns = ["Current", "Voltage", "ActivePwr", "ReactvPwr", "Frequency"]

#serial connect
ser = serial.Serial(port='/dev/tty.usbmodem141110',baudrate=115200, timeout=0.01)
#connect
print("connected to : " + ser.portstr)
#set single wire mode
print("set single wire mode:")
ACK = ser_write_single_mode(ser)
if len(ACK) == 0:
    print("fail... data length is 0")

#instance call
mydata = MCP39F511_SingleWireMode()

'''
plt.ion()

fig = plt.figure(figsize=(8,8))

ax1 = fig.add_subplot(511, ylim=[y1_min_value, y1_max_value], title="Current")
line1, = ax1.plot(init_x, init_y1, 'r-') # Returns a tuple of line objects, thus the comma
ax2 = fig.add_subplot(512, ylim=[y2_min_value, y2_max_value], title="Voltage")
line2, = ax2.plot(init_x, init_y2, 'b-') # Returns a tuple of line objects, thus the comma
ax3 = fig.add_subplot(513, ylim=[y3_min_value, y3_max_value], title="ActivePwr")
line3, = ax3.plot(init_x, init_y3, 'g-') # Returns a tuple of line objects, thus the comma
ax4 = fig.add_subplot(514, ylim=[y4_min_value, y4_max_value], title="ReactvPwr")
line4, = ax4.plot(init_x, init_y4, 'y-') # Returns a tuple of line objects, thus the comma
ax5 = fig.add_subplot(515, ylim=[y5_min_value, y5_max_value], title="Frequency")
line5, = ax5.plot(init_x, init_y5, 'm-') # Returns a tuple of line objects, thus the comma

text = ax1.text(ax1.get_xlim()[1]*0.7, ax1.get_ylim()[1]*1.2, "")

text1 = ax1.text(ax1.get_xlim()[1]*0.8, ax1.get_ylim()[1]*0.8, str(init_y1[-1]))
text2 = ax2.text(ax2.get_xlim()[1]*0.8, ax2.get_ylim()[1]*0.8, str(init_y2[-1]))
text3 = ax3.text(ax3.get_xlim()[1]*0.8, ax3.get_ylim()[1]*0.8, str(init_y3[-1]))
text4 = ax4.text(ax4.get_xlim()[1]*0.8, ax4.get_ylim()[1]*0.8, str(init_y4[-1]))
text5 = ax5.text(ax5.get_xlim()[1]*0.8, ax5.get_ylim()[1]*0.8, str(init_y5[-1]))

plt.tight_layout()
'''
start = time.time()

while(True):
    
    
    #get ["Current", "Voltage", "ActivePwr", "ReactvPwr", "Frequency"]
    processed_data = mydata.run(ser_read(ser))
    if processed_data != False:
        pass
    else:
        continue

    energy_data = [[processed_data[2], processed_data[3]]]
    pca0 = pca_training.transform(energy_data)[:,0]
    pca1 = pca_training.transform(energy_data)[:,1]
    features = [[processed_data[2], processed_data[3], pca0[0], pca1[0]]]
    #print(processed_data[2], processed_data[3], pca0[0], pca1[0])
    my_prediction = ETC.predict(features)
    
    print(key[int(my_prediction[0])])
    
    if(time.time() - start > 1):
        start = time.time()
        post = {"Current" : processed_data[0],
        "Voltage" : processed_data[1],
        "ActivePwr" : processed_data[2], 
        "ReactvPwr" : processed_data[3], 
        "Frequency" : processed_data[4],
        "label" : int(my_prediction[0]),
        "date": datetime.datetime.utcnow()}
        
        posts.insert_one(post)
        print("db_insert_complete")
        
        #remain 300 document
        if posts.count() > 500:
            remove_num = posts.count() - 300
            print(remove_num)
            doc = posts.find().sort("date", 1)
            thr = doc[remove_num]["date"]
            posts.delete_many({"date":{"$lt":thr}})
    
'''



    init_y = [init_y1, init_y2, init_y3, init_y4 ,init_y5]

    feature = delta(init_y3)
    _x = np.reshape(feature, (-1,sequence_length,input_dim))
    #result = sess.run(prediction, feed_dict={X:_x, batch_size:1})
    result, output_array = my_lstm.get_prediction(_x)
    #output_last = numpy_softmax(output_array[0][-1])


    #print(key[result[0][-1]], max(output_last))

    text.set_text("prediction : "+ key[result[0][-1]])

    init_y1 = init_y1[1:]+[processed_data[0]]
    init_y2 = init_y2[1:]+[processed_data[1]]
    init_y3 = init_y3[1:]+[processed_data[2]]
    init_y4 = init_y4[1:]+[processed_data[3]]
    init_y5 = init_y5[1:]+[feature[-1]]

    text1.set_text(init_y1[-1])
    text2.set_text(init_y2[-1])
    text3.set_text(init_y3[-1])
    text4.set_text(init_y4[-1])
    text5.set_text(feature[-1])

    line1.set_ydata(init_y1)
    line2.set_ydata(init_y2)
    line3.set_ydata(init_y3)
    line4.set_ydata(init_y4)
    line5.set_ydata(init_y5)
    fig.canvas.draw()

plt.close()
'''
ser.close()


# In[ ]:


#plt.close()

