var d3 = require("d3")
var jsdom = require("jsdom/lib/old-api");
var path = require('path');
var express = require("express");
var app = express();
var fs = require("fs");
var mongoose = require('mongoose'); //1. mongoose 모듈 가져오기
mongoose.connect('mongodb://localhost:27017/energy_data'); //2. energy_data setting 
var db = mongoose.connection;//3. 연결된 energy_data 사용
var Schema = mongoose.Schema;
var db_Schema = new Schema({
  _id: Object,
  Current:  Number,
  Voltage: Number,
  ActivePwr:   Number,
  ReactvPwr: Number,
  Frequency: Number,
  label: Number,
  date: Date
});


var energy_model = db.model("posts", db_Schema)

//var jqueryjs = fs.readFileSync("./jquery-3.2.1.min.js").toString();

app.use('/css',express.static(path.join(__dirname + '/css')));
app.use('/js',express.static(path.join(__dirname + '/js')));
app.use('/font-awesome',express.static(path.join(__dirname + '/font-awesome')));
app.use('/picture',express.static(path.join(__dirname + '/picture')));
app.use('/sql',express.static(path.join(__dirname + '/sql')));


app.listen(3303, function(){
	console.log("server start");
});



app.get("/req", function(req, res){

	/*
	energy_model.find({}, function(err, docs){
		if(err){
			console.log(err);
		}
		else{
			console.info('%d potatoes were successfully found.', docs.length);
			
		}	
	});
	*/

	var cursor = energy_model.find({}).sort({"date" : -1}).limit(50).cursor();
	//console.log(cursor)
	var active_power = [];
	var reactive_power = [];
	var current = [];
	var voltage = []; 
	var label = [];
	cursor.on('data', function(doc){
		active_power.push(doc.ActivePwr);
		reactive_power.push(doc.ReactvPwr);
		current.push (doc.Current); 
		voltage.push (doc.Voltage); 
		label.push(doc.label);
		
	});
	cursor.on('close', function(){

		active_power.reverse();
		reactive_power.reverse();

		

		res.send({
			"activePwr" : active_power,
			"reactivePwr" : reactive_power,
			"current": current, 
			"voltage": voltage, 
			"label": label[0]
		});
		
	});

});

app.get('/index.html', function(req, res){

	fs.readFile("index.html", function(error, data){
		jsdom.env({
			html: data,
			//src:[jqueryjs],
			done: function (errors, window){
				var $ = require('jquery')(window);
				//d3.select(window.document.getElementById("flot-moving-line-chart"))

				//$(window.document).ready(function(){	
				//	console.log("document ready");
				//});
				
				res.writeHead(200, {"Content-Type":"text/html"});
				res.end(data);
			}
		});
	});	

});
