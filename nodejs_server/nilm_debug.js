var d3 = require("d3")
var jsdom = require("jsdom/lib/old-api");
var path = require('path');
var express = require("express");
var http = require('http');
var app = express();
var fs = require("fs");
var mongoose = require('mongoose'); //1. mongoose 모듈 가져오기
mongoose.connect('mongodb://localhost:27017/energy_data'); //2. energy_data setting 
var db = mongoose.connection;//3. 연결된 energy_data 사용
var Schema = mongoose.Schema;
var db_Schema = new Schema({
  _id: Object,
  Current:  Number,
  Voltage: Number,
  ActivePwr:   Number,
  ReactvPwr: Number,
  Frequency: Number,
  label: Number,
  date: Date
});

var ActivePower = 0
var ReactivePower = 0
var label = 0


var energy_model = db.model("posts", db_Schema)

//var jqueryjs = fs.readFileSync("./jquery-3.2.1.min.js").toString();

app.use('/css',express.static(path.join(__dirname + '/css')));
app.use('/js',express.static(path.join(__dirname + '/js')));
app.use('/font-awesome',express.static(path.join(__dirname + '/font-awesome')));
app.use('/picture',express.static(path.join(__dirname + '/picture')));
app.use('/sql',express.static(path.join(__dirname + '/sql')));


app.listen(3303, function(){
	console.log("server start");
});



app.get("/req", function(req, res){



	var options = {
		hostname:'localhost',
		port:'8081',
		path:'/',
		method:'POST'
	};

	function handleResponse(response){
		var resPonseData = '';
		response.on('data', function(chunk){
			resPonseData += chunk;
		});
		response.on('end', function(){
			var dataObj = JSON.parse(resPonseData);
			ActivePower = dataObj.ActivePower
			ReactivePower = dataObj.ReactivePower
			console.log("response : ActivePower -> " + ActivePower[49] + " ReactivePower -> " + ReactivePower[49])
			label = dataObj.label

		});
	}

	http.request(options, function(response){
		handleResponse(response);
		res.send({
		"activePwr" : ActivePower,
		"reactivePwr" : ReactivePower,
		"label": label
		});
	}).end();

	
	//console.log(ActivePower);
	//console.log(ReactivePower);
	//console.log(label);

/*

*/

});

app.get('/index.html', function(req, res){

	fs.readFile("index.html", function(error, data){
		jsdom.env({
			html: data,
			done: function (errors, window){
				var $ = require('jquery')(window);
				res.writeHead(200, {"Content-Type":"text/html"});
				res.end(data);
			}
		});
	});	

});
